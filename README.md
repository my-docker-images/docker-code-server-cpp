# docker-code-server-cpp

## What

This image is based on [docker-code-server](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-code-server),
it adds the needed tools for C/C++ development.

Included tools are:
- g++-11 (as g++), gcc-11 (as gcc)
- clang++-14 (as c++), clang-14 (as cc)
- clangd with its [VSCode extension](https://marketplace.visualstudio.com/items?itemName=llvm-vs-code-extensions.vscode-clangd)
- gdb
- lldb
- VSCode [extension for lldb](https://github.com/vadimcn/vscode-lldb)
- [CMake](https://cmake.org)
- [Boost](www.boost.org)
- [GMP](https://gmplib.org)
- [OneTBB](https://spec.oneapi.io/versions/latest/elements/oneTBB/source/nested-index.html)
- [Valgrind](https://valgrind.org)
- [GoogleTest](https://github.com/google/googletest)


## Details

- The exposed port is 8443
- The user folder is `/config`
- the user and sudo password is `abc`
- if docker is installed on your computer, you can run this image, assuming you are in a specific folder that 
  will be shared with the container at `/config`, with:
  `docker run -p 8443:8443 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-code-server-cpp`

