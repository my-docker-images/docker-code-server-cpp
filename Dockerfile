
FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-code-server:latest

## software needed

# g++-12 is available on Ubuntu 22.04, but boost currently depends on g++-11
RUN apt-get update && apt-get install -y --no-install-recommends \
        clang \
        clangd \
        cmake \
        gdb \
        g++-11 \
        googletest \
        libboost-all-dev \
        libgmp-dev \
        libtbb-dev \
        lldb \
        make \
        valgrind && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN update-alternatives \
        --install /usr/bin/gcc  gcc   /usr/bin/gcc-11   100 \
        --slave   /usr/bin/g++  g++   /usr/bin/g++-11       && \
    update-alternatives \
        --install /usr/bin/cc   cc    /usr/bin/clang    100 && \
    update-alternatives \
        --install /usr/bin/c++  c++   /usr/bin/clang++  100

## Build googletest with installed compiler
RUN mkdir -p /tmp/gtest && \
    cd /tmp/gtest && \
    cmake /usr/src/googletest/googletest && \
    make all && \
    mv /tmp/gtest/lib/* /usr/lib/ && \
    mv /usr/src/googletest/googletest/include/gtest /usr/include/ && \
    cd / && \
    rm -r /tmp/gtest

# C++ extension

RUN /app/code-server/bin/code-server \
        --extensions-dir /init-config/extensions \
        --install-extension vadimcn.vscode-lldb

RUN /app/code-server/bin/code-server \
        --extensions-dir /init-config/extensions \
        --install-extension llvm-vs-code-extensions.vscode-clangd

RUN mkdir -p /init-config/.config/clangd
COPY config.yaml /init-config/.config/clangd/

RUN mkdir -p /init-config/workspace/.vscode
COPY  tasks.json /init-config/workspace/.vscode/
COPY launch.json /init-config/workspace/.vscode/

RUN mkdir -p /init-config/data/User
COPY settings.json /init-config/data/User/
